﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OnlineStoreBasic.Models;

namespace OnlineStoreBasic.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        MobileContext db;

        public HomeController(ILogger<HomeController> logger, MobileContext context)
        {
            _logger = logger;
            db = context;
        }

        public IActionResult Index()
        {
            return View(db.Phones.ToList());
        }

        public IActionResult Orders()
        {
            return View(db.Orders.ToList());
        }

        [HttpGet]
        public IActionResult Buy(int? id) {
            if (id == null)
                return RedirectToAction("Index");
            ViewBag.PhoneId = id;
          
            return View();
        }

        [HttpPost]
        public string Buy(Order order) {
            db.Orders.Add(order);
            db.SaveChanges();

            return "Дякую, " + order.User + ", за покупку!";
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
