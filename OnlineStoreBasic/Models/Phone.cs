﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineStoreBasic.Models
{
    public class Phone
    {
        /// <summary>
        /// Phone id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Phone name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Phone company name
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// Phone price
        /// </summary>
        public int Price { get; set; }
    }
}
