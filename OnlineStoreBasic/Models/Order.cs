﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineStoreBasic.Models
{
    public class Order
    {
        /// <summary>
        /// Order id
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Order user
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// Order user adress
        /// </summary>
        public string Adress { get; set; }
        
        /// <summary>
        /// User contact phone
        /// </summary>
        public string ContactPhone { get; set; }

        /// <summary>
        /// Order phone id
        /// </summary>
        public int PhoneId { get; set; }

        /// <summary>
        /// Order phone object
        /// </summary>
        public Phone Phone { get; set; }

    }
}
